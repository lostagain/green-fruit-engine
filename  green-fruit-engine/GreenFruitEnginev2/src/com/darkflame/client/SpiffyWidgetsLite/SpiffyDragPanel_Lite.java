package com.darkflame.client.SpiffyWidgetsLite;

import java.util.ArrayList;
import java.util.logging.Logger;

import javax.xml.bind.annotation.DomHandler;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.dom.client.TouchCancelEvent;
import com.google.gwt.event.dom.client.TouchCancelHandler;
import com.google.gwt.event.dom.client.TouchEndEvent;
import com.google.gwt.event.dom.client.TouchEndHandler;
import com.google.gwt.event.dom.client.TouchMoveEvent;
import com.google.gwt.event.dom.client.TouchMoveHandler;
import com.google.gwt.event.dom.client.TouchStartEvent;
import com.google.gwt.event.dom.client.TouchStartHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Essentially this is an absolute panel that can be dragged about by the mouse,
 * and have elements added too it with full muse actions.
 * Its a cut down version from Thomas Wrobels personal widget lib
 * Sorry for the mess
 * -Thomas
 **/

public class SpiffyDragPanel_Lite extends FocusPanel implements MouseWheelHandler,
		MouseOverHandler, MouseOutHandler,  TouchCancelHandler {
	
	SpiffyDragPanel_Lite thisDragPanel = this;
	
	AbsolutePanel Container = new AbsolutePanel();
	// for debugging
	DragPanelDatabar databar = new DragPanelDatabar();

	int ContainerSizeX = 0;
	int ContainerSizeY = 0;

	//static int EdgePaddingForRestrictToScreen = 20;
	
	public AbsolutePanel dragableContents = new AbsolutePanel();

	// loading widget overlay
	SimplePanel loadingOverlay = new SimplePanel();
	VerticalPanel loadingMessage = new VerticalPanel();
	// --

	int dragStartX = 0, dragStartY = 0;
	int top = 0, left = 0;
	Boolean isMoving = false;
	Boolean isCoasting = false;
	

	private boolean hardStop = false;
	int locationdownx = 0;
	int locationdowny = 0;
	long dragstart = System.currentTimeMillis();
	Timer motionflow;

	// frame update time
	static final int FRAME_RATE_UPDATE_TIME = 50; // 50 is safe

	private double MotionDisX = 0;
	private double MotionDisY = 0;

	static Logger Log = Logger.getLogger("SpiffyDragPanel_l");
	private int Max_Height = -10000;// default (these arnt used to really check
									// the height, as the container size has to
									// be taken into account)
	private int Max_Width = -10000;// default

	private int MaxXMovement = 0;
	private int MaxYMovement = 0;
	private int MinXMovement = 0;
	private int MinYMovement = 0;

	static boolean draging = false;
	//static boolean justDragged= false;
	Timer quickfade;

	public ArrayList<FocusPanel> allObjectsOnPanel = new ArrayList<FocusPanel>();
	private SpiffyLoadingIcon_Lite LoadingIcon;

	Label messageLabel;
	boolean progressLabelOn = false;


	public SpiffyDragPanel_Lite() {

		super.setWidget(Container);

		Log.info("setMovementLimits0"); 
		Container.add(dragableContents, 0, 0);


		Container.setSize("100%", "100%");
		dragableContents.setSize("100%", "100%");
		
		this.addMouseMoveHandler(new SpiffyDragMouseMoveHandler());
		this.addMouseUpHandler(new SpiffyDragMouseUpHandler());
		this.addMouseDownHandler(new SpiffyDragMouseDownHandler(null));
		
		this.addMouseOutHandler(this);
		this.addMouseOverHandler(this);
		this.addMouseWheelHandler(this);
		
		this.addTouchEndHandler(new SpiffyDragTouchEndHandler());
		this.addTouchMoveHandler(new SpiffyDragTouchMoveHandler());
		this.addTouchStartHandler(new SpiffyDragTouchDownHandler(null));

		
		
		// quick fadeout
		quickfade = new Timer() {
			int o = 100;

			@Override
			public void run() {
				o = o - 10;

				loadingOverlay.getElement().getStyle().setOpacity(o / 100.0);

				if (o < 10) {
					Container.remove(loadingOverlay);

					loadingOverlay.clear();
					loadingMessage.clear();
					this.cancel();
				}
			}
		};

		motionflow = new MotionFlowTimer();
		
		

	}

	public int getCurrentPanelAbsoluteX(){
		return dragableContents.getAbsoluteLeft();
	}
	
	public int getCurrentPanelAbsoluteY(){
		return dragableContents.getAbsoluteTop();
	}
	

	
	public void setViewToPos(int X, int Y) {
		Log.info("::::::::::::setting view to pos x/y" + X + "," + Y);

	
			left = -X + (ContainerSizeX / 2);


			top = -(Y - (ContainerSizeY / 2));


		Log.info("::::::::::::setting view to " + left + ", " + top);

		Log.info("::::::::::::ContainerSizeY = " + ContainerSizeY);

		Log.info("::::::::::::ContainerSizeX = " + ContainerSizeX);
		
		setPositionInternalCoOrdinates(left, top);
	}

	/**
	 * sets the movement limits for the panning imagine it as the small box
	 * inside the bigger box...the user wont be able to move outside the small
	 * box, but might see a little outside it when the movement bounces at the
	 * edges
	 * **/
	public void setMovementLimits(int StartX, int StartY, int endX, int endY) {

		// set top left

		// set bottom right
		Max_Height = endY;
		Log.info("Max_Height= " + Max_Height + "");

		Max_Width = endX;
		Log.info("Max_Width= " + Max_Width + "");

		ContainerSizeX = Container.getOffsetWidth();
		ContainerSizeY = Container.getOffsetHeight();

		Log.info("_containersizeX = " + ContainerSizeX);
		Log.info("_containersizeY = " + ContainerSizeY);

		MaxXMovement = Max_Width - ContainerSizeX;
		MaxYMovement = Max_Height - ContainerSizeY;

		MinXMovement = StartX;
		MinYMovement = StartY;

		updateDragableSize();
	}

	public void setInternalSize(int x, int y) {

		dragableContents.setSize(x + "px", y + "px");

		// if the container size is bigger then the contents, then centre and
		// disable movement in that direction
		updateDragableSize();
		

	}

	public void updateDragableSize() {

		Log.info("updateDragableSize");

		ContainerSizeX = Container.getOffsetWidth();
		ContainerSizeY = Container.getOffsetHeight();

		MaxXMovement = Max_Width - ContainerSizeX;
		MaxYMovement = Max_Height - ContainerSizeY;

		int x = dragableContents.getOffsetWidth();
		int y = dragableContents.getOffsetHeight();

		Log.info("::::::::::::::::::::::::testing for size ____ x=" + x
				+ "___ containerx=" + ContainerSizeX);

		if (ContainerSizeX > x) {

			left = ((ContainerSizeX / 2) - (x / 2));

			Log.info("centering x=" + left);

			// Container.setWidgetPosition(dragableContents, left ,top);
			

		}

		if (ContainerSizeY > y) {

			top = ((ContainerSizeY / 2) - (y / 2));

			Log.info("centering y " + top);
			// Container.setWidgetPosition(dragableContents, left , top);
		

		}

		setPositionInternalCoOrdinates(left, top);
	}

	@Override
	public void onLoad() {

		ContainerSizeX = Container.getOffsetWidth();
		ContainerSizeY = Container.getOffsetHeight();

		Log.info("_containersizeX = " + ContainerSizeX);
		Log.info("_containersizeY = " + ContainerSizeY);

		MaxXMovement = Max_Height - ContainerSizeX;
		MaxYMovement = Max_Width - ContainerSizeY;

		super.onLoad();

	}
	
	

	/** sets the overall background colour **/
	public void setBackgroundColour(String css) {
		
		
		Log.info("css = "+css);
		Container.getElement().getStyle().setBackgroundColor(css);
		

	}

	
	
	/** sets the css background **/
	public void setDraggableBackground(String url) {
		Log.info("url(\"" + url + "\")");
		dragableContents.getElement().getStyle()
				.setBackgroundImage("url(\"" + url + "\")");

	}

	
	public void setLoadingCounter(boolean status) {
		
		// set loading data
		progressLabelOn = status;

		LoadingIcon.setProgressLabelVisible(progressLabelOn);

	}

	/** sets the loading overlay on/off **/
	public void setLoading(boolean status, String Message) {

		messageLabel = new Label(Message);

		if (status) {

			Container.add(loadingOverlay, 0, 0);
			loadingOverlay.getElement().getStyle().setBackgroundColor("#000");
			loadingOverlay.getElement().getStyle().setZIndex(99999);
			loadingOverlay.getElement().getStyle().setColor("#FFF");

			loadingOverlay.setSize("100%", "100%");
			LoadingIcon = new SpiffyLoadingIcon_Lite(false);
			LoadingIcon.setProgressLabelVisible(true);
			loadingMessage.getElement().getStyle().setColor("#FFF");
			loadingMessage.setVerticalAlignment(HasVerticalAlignment.ALIGN_TOP);
			loadingMessage
					.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);

			loadingMessage.setSpacing(7);
			loadingMessage.add(messageLabel);
			loadingMessage.add(LoadingIcon);
			
			loadingMessage.setSize("100%", "100%");
			loadingMessage.setHorizontalAlignment(HasAlignment.ALIGN_CENTER);
			
			loadingOverlay.add(loadingMessage);

		} else {

			LoadingIcon.stopAnimation();
			LoadingIcon.clear();

			quickfade.scheduleRepeating(50);

		}

	}
	
	public void stepLoading() {
		LoadingIcon.stepProgressForward();
	}

	public void setLoadingTotal(int T) {
		LoadingIcon.setTotalUnits(T);
	}


	public void setWidgetsPosition(Widget widget, int x, int y,boolean restrictToScreen) {
		
		// we get the parent, as its in a container widget
		dragableContents.setWidgetPosition(widget.getParent(), x, y);
		

	}
	
	public void removeWidget(Widget widget){
	
		//first we get the parent, as all widgets added to the panel should be contained in a containerWidget
		FocusPanel containerWidget = (FocusPanel) widget.getParent();
		
		Log.info("_removing object from spiffyDragPanel which currently has "+dragableContents.getWidgetCount());
		Boolean success = dragableContents.remove(containerWidget);
		Boolean success2 =allObjectsOnPanel.remove(containerWidget);
		Log.info("_removing_"+success+"_"+success2);
		
		
	}
	
	public boolean isOnPanel(Widget widget){

		//first we get the parent, as all widgets added to the panel should be contained in a containerWidget
		FocusPanel containerWidget;
		try {
			//first we see if the widgets parent is a FocusPanel, as all the panels widgets are contained by them.
			containerWidget = (FocusPanel) widget.getParent();
			
		} catch (Exception e) {
			// if not, then we return false
			return false;
		}
		
		
		int wi = dragableContents.getWidgetIndex(containerWidget);
		
		//if its greater then -1, then its contained on this panel
		if (wi>-1){
			return true;
		}
		//else it isnt
		return false;

		
	}
	
	/** adds a widget to the top left of the drag panel **/
	@Override	
	public void add(Widget widgetToAddAtTopLeft){
		addWidget(widgetToAddAtTopLeft, 0, 0);
	}

	/** adds a widget to the position x,y in the drag panel
	 * the widget is first added to a container, however, to ensure clicks and drags are handled correctlu 
	 * **/
	public void addWidget(Widget widget, int x, int y) {

		// container widget
		FocusPanel containerWidget = new FocusPanel();
		containerWidget.add(widget);

		dragableContents.add(containerWidget, x, y);
		
		Log.info("widget added at:"+x+","+y);
		Log.info("widget added at:"+containerWidget.getElement().getStyle().getLeft()+","+containerWidget.getElement().getStyle().getTop());

		// we have to add our own handlers to the widget to deal with dragging
		// correctly
		containerWidget.addMouseDownHandler(new SpiffyDragMouseDownHandler(widget));
		containerWidget.addMouseMoveHandler(new SpiffyDragMouseMoveHandler());
		containerWidget.addMouseUpHandler(new SpiffyDragMouseUpHandler());
		containerWidget.addMouseOutHandler(this);

		containerWidget.addMouseOverHandler(this);
		containerWidget.addMouseWheelHandler(this);

		containerWidget.addTouchEndHandler(new SpiffyDragTouchEndHandler());
		containerWidget.addTouchMoveHandler(new SpiffyDragTouchMoveHandler());
		containerWidget.addTouchStartHandler(new SpiffyDragTouchDownHandler(widget));
		containerWidget.addTouchCancelHandler(this);
		
		allObjectsOnPanel.add(containerWidget);

	}
	
	public void clearAllWidgets(){
		
		for (Widget widgetOnPanel : allObjectsOnPanel) {
			widgetOnPanel.removeFromParent();	
			
		}
		
		allObjectsOnPanel.clear();
		
	}
	
	@Override
	/** for this panel to work, widgets go on a subpanel within it. We dont want this subpanel removed, hence the clear command
	 * just redirects to the clearAllWidgets command, that removes the inner widgets only**/
	public void clear(){
		clearAllWidgets();
	}
	
	class SpiffyDragTouchMoveHandler implements TouchMoveHandler {


		public SpiffyDragTouchMoveHandler() {

		}


		@Override
		public void onTouchMove(TouchMoveEvent event) {
			
			History.newItem("Touch Move Event Detected!");
			Window.setStatus("Touch Move Event");
		
			event.stopPropagation();
			event.preventDefault();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			
			int x = event.getTouches().get(0).getRelativeX(event.getRelativeElement());
			int y = event.getTouches().get(0).getRelativeY(event.getRelativeElement());
			
			Log.info("_____Touch Move Event Detected");
			onMouseOrTouchMove(x, y);
		}
		
	}	
		
		
	class SpiffyDragMouseMoveHandler implements MouseMoveHandler {


		public SpiffyDragMouseMoveHandler() {

		}

		@Override
		public void onMouseMove(MouseMoveEvent event) {
			event.preventDefault();
			event.stopPropagation();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			
			//int x = event.getX();
			//int y = event.getY();
			//fix?
			int x = event.getRelativeX(Container.getElement());
			int y = event.getRelativeY(Container.getElement());
			
			onMouseOrTouchMove(x, y);
		}

	}
	class SpiffyDragTouchDownHandler implements TouchStartHandler {

	//	private boolean fromItem = false;

		private Widget sourceWidget = null;
		

		public SpiffyDragTouchDownHandler(
				Widget widget) {
			

			//this.fromItem = fromItem;
			this.sourceWidget = widget;

		}

		@Override
		public void onTouchStart(TouchStartEvent event) {

			History.newItem("Touch start Event Detected!");
			Window.setStatus("Touch start Event");

			// This will stop the event from being
			// propagated
			event.stopPropagation();
			event.preventDefault();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			DOM.setCapture(((Widget) event.getSource()).getElement());
			
			//Log.info("______onMouseDown");
			
			int x = event.getTouches().get(0).getRelativeX(event.getRelativeElement());
			int y = event.getTouches().get(0).getRelativeY(event.getRelativeElement());

			int dx = event.getTouches().get(0).getRelativeX(sourceWidget.getElement());
			int dy = event.getTouches().get(0).getRelativeY(sourceWidget.getElement());
			
			onMouseOrTouchDown(x, y, dx, dy,sourceWidget);

		}

		
	}
	class SpiffyDragMouseDownHandler implements MouseDownHandler {

		//private boolean fromItem = false;

		private Widget sourceWidget = null;
		

		public SpiffyDragMouseDownHandler(
				Widget widget) {
			

		//	this.fromItem = fromItem;
			this.sourceWidget = widget;

		}

		@Override
		public void onMouseDown(MouseDownEvent event) {
			event.preventDefault();
			DOM.setCapture(((Widget) event.getSource()).getElement());
			// This will stop the event from being
			// propagated
			event.stopPropagation();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			
			
			Log.info("______onMouseDown");
			
			//int x = event.getX();
			//int y = event.getY();
			//fix?
			int x = event.getRelativeX(Container.getElement());
			int y = event.getRelativeY(Container.getElement());
			
			int dx = 0;
			int dy =0;
			
			
			onMouseOrTouchDown(x, y, dx, dy,sourceWidget);

		}

		
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		event.preventDefault();

		if (draging == true) {
			draging = false;
			//justDragged=false;
			// motion flow
			motionflow();
		}
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		draging = false;
		//justDragged=false;
		event.preventDefault();

	}

	@Override
	public void onMouseWheel(MouseWheelEvent event) {
		event.preventDefault();
	}


	

	class SpiffyDragMouseUpHandler implements MouseUpHandler {

		//private boolean sourceWasItem = false;

		public SpiffyDragMouseUpHandler() {
			//sourceWasItem = fromItem;
			//Log.info("set up_:" + sourceWasItem + " ");

		}

		@Override
		public void onMouseUp(MouseUpEvent event) {
			event.preventDefault();
			// This will stop the event from being
			// propagated
			event.stopPropagation();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			DOM.releaseCapture(((Widget) event.getSource()).getElement());

			onMouseOrTouchUp();
	

	
		}

	}

	class SpiffyDragTouchEndHandler implements TouchEndHandler {


		public SpiffyDragTouchEndHandler() {

		}

		@Override
		public void onTouchEnd(TouchEndEvent event) {
			event.preventDefault();
			// This will stop the event from being
			// propagated
			event.stopPropagation();
			DOM.eventCancelBubble(DOM.eventGetCurrentEvent(), true);
			DOM.releaseCapture(((Widget) event.getSource()).getElement());

			onMouseOrTouchUp();
		}

	}
	private void motionflow() {


		int displacementX = left - locationdownx;
		int displacementY = top - locationdowny;
		long period = System.currentTimeMillis() - dragstart;

		// displacement per unit of time;
		MotionDisX = ((double) displacementX / (double) period) * 50;
		MotionDisY = ((double) displacementY / (double) period) * 50;


		// motionflow.cancel();
		// only start if not already running
		if (!(isCoasting)) {
			motionflow.scheduleRepeating(FRAME_RATE_UPDATE_TIME);
		} else {
			Log.info("\n already coasting, so no new motion flow needed!");
		}

	}

	private void setPositionInternalCoOrdinates(int setX, int setY) {

		// NEW: Move only the layer
		Container.setWidgetPosition(dragableContents, setX, setY);

		 Log.info("coordinates set");
		databar.setCurrentPositionLabel(-setX, -setY);

	}

	private void displacebyInternalCoOrdinates(int disX, int disY) {

		// make sure X/Y isn't outside boundary's
		if ((left > -MinXMovement)  ) {
			left = -MinXMovement;
		}
		;
		if ((top > -MinYMovement) ) {
			top = -MinYMovement;
		}
		;


		// stop movement at bottom right limits
		if ((top < -MaxYMovement)) {
			// Log.info("hit height:" + Max_Height);
			top = -MaxYMovement;
		}
		;

		if ((left < -MaxXMovement)) {
			// Log.info("hit width" + Max_Width);
			left = -MaxXMovement;
		}
		;

		// get new co-ordinates based on old ones
		left = left + disX;
		top = top + disY;

		setPositionInternalCoOrdinates(left, top);

	}

	private final class MotionFlowTimer extends Timer {
		@Override
		public void run() {

			if (hardStop) {
				Log.info("hard stopping");
				isCoasting = false;
				motionflow.cancel();
				return;
			}

			// set in motion flag;
			isCoasting = true;

			// Log.info("seting coordinates from timer:");

			displacebyInternalCoOrdinates((int) Math.round(MotionDisX),
					(int) Math.round(MotionDisY));

			// Log.info("set coordinates from timer_0");

			// slow down
			MotionDisX = (MotionDisX / 1.2);
			MotionDisY = (MotionDisY / 1.2);

			// stop
			isMoving = false;
			if ((MotionDisX < 1) && (MotionDisX > -1)) {
				MotionDisX = 0;
			} else {
				isMoving = true;
			}
			if ((MotionDisY < 1) && (MotionDisY > -1)) {
				MotionDisY = 0;
			} else {
				isMoving = true;
			}

			if (!(isMoving)) {
				this.cancel();
				Log.info("\n stoped");
				isCoasting = false;

			}

		}

	}

	/** gives useful debugging data **/
	static class DragPanelDatabar extends HorizontalPanel {

		// current mouse position
		Label lab_currentMouseX = new Label("-");
		Label lab_currentMouseY = new Label("-");

		// current canvas position
		Label lab_currentXpos = new Label("-");
		Label lab_currentYpos = new Label("-");

		public DragPanelDatabar() {
			// fill container horizontal
			this.setWidth("20%");
			// set background to blacks
			
			this.getElement().getStyle().setColor("WHITE");
			this.getElement().getStyle().setProperty("textShadow", "rgb(0, 0, 0) 0px 0px 3px");
			this.getElement().getStyle().setZIndex(900000);

			addMouseFeedback();
		}

		public void setCurrentPositionLabel(int X, int Y) {

			lab_currentXpos.setText("x = " + X + "");
			lab_currentYpos.setText("y = " + Y + "");
		}

		public void setCurrentMousePositionLabel(int X, int Y) {

			lab_currentMouseX.setText("mx = " + X + "");
			lab_currentMouseY.setText("my = " + Y + "");
		}

		private void addMouseFeedback() {
			// add mouse feedback

			VerticalPanel mouseLoc = new VerticalPanel();
				
			mouseLoc.add(lab_currentMouseX);
			mouseLoc.add(lab_currentMouseY);

			lab_currentMouseX.setStylePrimaryName("FeedbackLabel");
			lab_currentMouseY.setStylePrimaryName("FeedbackLabel");

			mouseLoc.setWidth("80px");
			super.add(mouseLoc);
			this.setCellWidth(mouseLoc, "90px");

			VerticalPanel canvasLoc = new VerticalPanel();
			canvasLoc.add(lab_currentXpos);
			canvasLoc.add(lab_currentYpos);
			canvasLoc.setWidth("60px");

			lab_currentXpos.setStylePrimaryName("FeedbackLabel");
			lab_currentYpos.setStylePrimaryName("FeedbackLabel");

			this.add(canvasLoc);
			this.setCellWidth(canvasLoc, "90px");

		}
	}








	public void DisplayDebugBar(boolean b) {
		if (b) {						
			Container.add(databar, 0, 0);
		} else {
			Container.remove(databar);
		}

	}

	private void onMouseOrTouchDown(int x, int y, int dx, int dy,Widget sourceWidget) {
		
		

		dragStartX = x - left;
		dragStartY = y - top;

		draging = true;

		locationdownx = left;
		locationdowny = top;
		dragstart = System.currentTimeMillis();
	}

	private void onMouseOrTouchMove(int x, int y) {
		

		
		databar.setCurrentMousePositionLabel(x - left, y - top);

		if (draging == true) {
		
			
				left = x - dragStartX;
				top = y - dragStartY;

			// make sure X/Y isn't outside top left boundaries
			
			if ((left > 0) ) {

				Log.info("___________left outside range"+left);

				left = 0;
			}
			;
			if ((top > 0) ) {
				top = 0;
			}
			;

			// make sure X/Y isn't outside boundaries
			if ((left < -MaxXMovement) ) {

				Log.info("____________left outside range 2");
				left = -MaxXMovement;
			}
			;
			if ((top < -MaxYMovement) ) {
				top = -MaxYMovement;
			}
			;
			// Log.info("setting co-ordinates");
			setPositionInternalCoOrdinates(left, top);
		}
	}

	private void onMouseOrTouchUp() {
				Log.info("mouse up..");

		if (draging == true) {
			draging = false;
			//justDragged=false;
			// motion flow
			motionflow();
		}
				
				


	}

	
	@Override
	public void onTouchCancel(TouchCancelEvent event) {
		event.preventDefault();
		 Log.info("touch drag canceled");

		onMouseOrTouchUp();
		
	}

	
}

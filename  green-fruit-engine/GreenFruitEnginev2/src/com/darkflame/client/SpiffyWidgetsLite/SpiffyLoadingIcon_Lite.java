package com.darkflame.client.SpiffyWidgetsLite;


import com.darkflame.client.interfaces.GenericProgressMonitor;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/** basic version of a loading icon, pulled from a currently unpublished
 * Library of widgets by Thomas Wrobel 
 * 
 * Note; does not clear itself correctly if total units goes up
 * */
public class SpiffyLoadingIcon_Lite extends VerticalPanel implements GenericProgressMonitor{

	//default size and padding
	int width = 70;
	int height = 70;
	int padding = 3;
	
	Canvas icon = Canvas.createIfSupported();
	CanvasElement maine = icon.getCanvasElement();
	Context2d drawplane = maine.getContext2d();

	Timer clock;
	double ANG = -Math.PI / 2;
	boolean autoRun = true;

	int TotalUnitsToLoad = 0;
	double StepSize = 0;

	int currentStep = 0;

	Label progressLab = new Label("");
	String CurrentProcess="";
	
	boolean debugData=true;
	Label LoadingTime = new Label("Loading loading time...");
	long startTime=0;
	public long currentTime=0;
	
	public SpiffyLoadingIcon_Lite(boolean autoRun) {
		this.autoRun = autoRun;

		if (icon == null) {
			return;
		}
		this.setSize((width + padding) + "px", (height + padding) + "px");
		icon.setWidth(width + "px");
		icon.setHeight(height + "px");
		icon.setCoordinateSpaceWidth(width);
		icon.setCoordinateSpaceHeight(height);

		this.add(icon);
		this.add(progressLab);
		this.add(LoadingTime);
		startTime = System.currentTimeMillis();
		
		
		progressLab.setVisible(false);
		
		clock = new Timer() {

			@Override
			public void run() {

				// Optimisation might be possible by drawing just a line and
				// using no fill at all

				ANG = ANG + 0.1;
				drawplane.beginPath();
				drawplane.setFillStyle(CssColor.make(0, 0, 150));
				drawplane.setStrokeStyle(CssColor.make(0, 0, 150));
				drawplane.moveTo(width / 2, height / 2);
				drawplane.arc(width / 2, height / 2, width / 2, -Math.PI / 2,
						ANG, false);
				drawplane.lineTo(width / 2, height / 2);
				// drawplane.closePath();
				drawplane.stroke();
				drawplane.fill();

				if (ANG > (Math.PI * 2) - Math.PI / 2) {
					// clear and restart
					ANG = -Math.PI / 2;
					drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(),
							icon.getCoordinateSpaceHeight());
				}
				if (debugData){
					
					currentTime= System.currentTimeMillis()-startTime;				
					LoadingTime.setText(":"+currentTime+":");
					
				}
				
			}

		};

	}

	@Override
	public void setPixelSize(int x,int y){

		width = x;
		height = y;
		setSize((width + padding) + "px", (height + padding) + "px");
		icon.setWidth(width + "px");
		icon.setHeight(height + "px");
		icon.setCoordinateSpaceWidth(width);
		icon.setCoordinateSpaceHeight(height);
		
	}
	
	@Override
	public void onAttach() {

		ANG = -Math.PI / 2;
		// clock.run();
		if (autoRun) {
			clock.scheduleRepeating(200);
		}
		super.onAttach();
		startTime = System.currentTimeMillis();
	}

	public void stopAnimation() {
		clock.cancel();
	}

	public void reset() {
		ANG = -Math.PI / 2;
		drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(),
				icon.getCoordinateSpaceHeight());
		startTime = System.currentTimeMillis();

	}

	public void startAnimation() {
		ANG = -Math.PI / 2;
		clock.cancel();
		clock.scheduleRepeating(200);
	}

	/** sets the total "units" to be loaded, ie, number of images, files etc **/
	public void setTotalUnits(int setTotalUnitsToLoad) {
		TotalUnitsToLoad = setTotalUnitsToLoad;

		// calculate the step needed when advancing
		StepSize = (Math.PI * 2) / TotalUnitsToLoad;
		

		//update image
		updateImage();

	}
	
	/** adds one to total "units" to be loaded, ie, number of images, files etc **/
	public void addToTotalProgressUnits(int addThis) {
		TotalUnitsToLoad = TotalUnitsToLoad+addThis;

		// calculate the step needed when advancing
		StepSize = (Math.PI * 2) / TotalUnitsToLoad;
		
		//update image
		updateImage();

	}
	public void setProgressLabelVisible(boolean status){

		progressLab.setVisible(status);
	}

	public void setCurrentProcess(String currentProcess) {
		CurrentProcess = currentProcess;
	}

	public void stepProgressForward() {

		currentStep++;

		updateImage();
		
		//update status text
		this.setTitle(currentStep+" out of "+TotalUnitsToLoad);
		
		
	}

	public void updateImage() {
		//update lab
		progressLab.setText(CurrentProcess+":"+currentStep+" / "+TotalUnitsToLoad+":");
		
		// ANG = ANG + StepSize;

		// work out percentage complete
		ANG = ((currentStep * 1.0) / (TotalUnitsToLoad*1.0)) * (Math.PI * 2);

		// TotalUnitsToLoad/current step

		drawplane.beginPath();
		drawplane.setFillStyle(CssColor.make(0, 0, 150));
		drawplane.setStrokeStyle(CssColor.make(0, 0, 150));
		drawplane.moveTo(width / 2, height / 2);
		drawplane.arc(width / 2, height / 2, width / 2, -Math.PI / 2, ANG-(Math.PI / 2),
				false);
		drawplane.lineTo(width / 2, height / 2);
		// drawplane.closePath();
		drawplane.stroke();
		drawplane.fill();

		if (ANG > (Math.PI * 2)) {
			// clear and restart
			ANG = 0; //-Math.PI / 2;
			drawplane.clearRect(0, 0, icon.getCoordinateSpaceWidth(),
					icon.getCoordinateSpaceHeight());
			currentStep = 0;
		}

		if (debugData){
			currentTime= System.currentTimeMillis()-startTime;				
			LoadingTime.setText("Time:"+currentTime+"");
			}
	}

	@Override
	public void setTotalProgressUnits(int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCurrentProgress(int i) {
		// TODO Auto-generated method stub
		
	}

}

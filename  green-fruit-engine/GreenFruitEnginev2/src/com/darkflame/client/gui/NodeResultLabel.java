package com.darkflame.client.gui;

import java.util.ArrayList;
import java.util.Iterator;

import com.darkflame.client.semantic.SSSNode;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;

public class NodeResultLabel extends Composite {

	Label DeducedFrom;
	Label EquivilentURIs;
	Label ParentsURI;
	Label DirectChildURI;
	
	public NodeResultLabel(SSSNode node) {

		// replace header in future with one that gives more information
		DisclosurePanel disclosurePanel = new DisclosurePanel(node.getPLabel());
		disclosurePanel.setOpen(true);
		
		initWidget(disclosurePanel);
		
		disclosurePanel.setTitle(node.getPURI()+"  ("+node.getAllPLabels()+")  ");
		
		
		disclosurePanel.setWidth("100%");

		VerticalPanel ResultDetails = new VerticalPanel();
		disclosurePanel.setContent(ResultDetails);
		ResultDetails.setSize("100%", "4cm");

		EquivilentURIs = new Label("Equivilent: "
				+ node.getEquivilentsAsString());
		ResultDetails.add(EquivilentURIs);

		ParentsURI = new Label("Parents: ");// +node.getDirectParentsAsString());
		HorizontalPanel parentNodes = new HorizontalPanel();

		parentNodes.add(ParentsURI);
		ResultDetails.add(parentNodes);
		
		HorizontalPanel childNodes = new HorizontalPanel();
		DirectChildURI= new Label("Direct Children: ");
		childNodes.add(DirectChildURI);
		childNodes.add(new Label(node.getDirectChildrenAsString()));
		
		ResultDetails.add(childNodes);
		// add parents
		ArrayList<SSSNode> kdpc = node.getKnownDirectParentClasses();
		Iterator<SSSNode> kdpcit = kdpc.iterator();
		while (kdpcit.hasNext()) {

			SSSNode sssNode = (SSSNode) kdpcit.next();
			
			//skip self
			if (sssNode!=node){
			
			parentNodes.add(new NodeResultLabel(sssNode));
			}
		}

		DeducedFrom = new Label("DeducedFrom");
		ResultDetails.add(DeducedFrom);
		
		disclosurePanel.setOpen(false);
	}

}

package com.darkflame.client.gui;

import com.darkflame.client.SuperSimpleSemantics;
import com.darkflame.client.semantic.SSSIndex;
import com.darkflame.client.semantic.SSSNodesWithCommonProperty;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.TabLayoutPanel;

/** the pages overall GUI **/
public class OverallInterface extends TabLayoutPanel {

	 /** The main interface to enter querys and display the results**/
	static QueryInterface queryInterface =new QueryInterface();
	
	/** the green fruit engines main debug logger **/
	public static DebugDisplayer debugDisplayer = new DebugDisplayer();
	
	//various information tabs
	static Frame wikipagecontainer = new Frame("./Docs/explanation_page.html");
	
	static CommonPropertyListDisplayer cplDisplayer = new CommonPropertyListDisplayer();
	static SSSIndexDisplayer sssIndexDisplayer = new SSSIndexDisplayer();
	static PrefixDisplayer prefixDisplayer = new PrefixDisplayer();
	
	
	public OverallInterface() {
		super(28, Unit.PX);		
		
		//set size to the max
		setSize("100%","100%");
		
		//add the subpages
		forceLayout(); 
		
		//first the main interface
		add(queryInterface,"Query");
		
		//the help tab
		add(wikipagecontainer,"Help & About");
		wikipagecontainer.setSize("100%","100%");
		
		//then the subpages
		add(sssIndexDisplayer,"Loaded Indexs");
		add(cplDisplayer,"Loaded Property Lists");
		add(prefixDisplayer,"Known Prefixs");		
		add(debugDisplayer,"Debug Stuff");
		
		//set to first tab
		selectTab(0);
		
		//set debug displayer in supersimplesemantics core
		//GreenFruitEnginev3.debugManager = debugDisplayer;
		SuperSimpleSemantics.setDebugBox(debugDisplayer);
		
		//set property list displayer in core
		SuperSimpleSemantics.setGenericPropertyListDisplayer(cplDisplayer);

		//set the index displayer
		SSSIndex.setIndexDisplayer(sssIndexDisplayer);
		
		
		
	}

	static public void clearIndexDisplayer(){		
		sssIndexDisplayer.clear();		
	}
	
	static public void addIndexToDisplayer(SSSIndex currentIndex){		
		sssIndexDisplayer.addIndexToDisplayer(currentIndex);		
		
	}

	//public static void addCPLToDisplayer(SSSNodesWithCommonProperty thisset) {
		
		//cplDisplayer.addCPLToDisplayer(thisset);
		
		//SuperSimpleSemantics.addCPLToDisplayer(thisset);
		
		
	//}

	public static void clearLists() {
		 clearIndexDisplayer();
		
	}

	public void postLoadInterfaceUpdates() {
		
		queryInterface.postLoadInterfaceUpdates();
		
	}


	

	@Override
	public void onResize(){
		super.onResize();
		queryInterface.onResize();
	}
	


	
}

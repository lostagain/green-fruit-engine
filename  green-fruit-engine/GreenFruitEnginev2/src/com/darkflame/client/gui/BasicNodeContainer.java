package com.darkflame.client.gui;

import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSProperty;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.WhiteSpace;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;


/**   **/
public class BasicNodeContainer extends FocusPanel {

	
	private static Boolean ColourFixed = false;
	

	//
	VerticalPanel dataHolder = new VerticalPanel();
			
	//Label nodeURI=new Label();
	Hyperlink nodePredURI = new Hyperlink();	
	Hyperlink nodeValueURI = new Hyperlink();	
	
	Label nodeLabel=new Label();

	BasicNodeContainer thisContainer = this;
	
	
	//int maxZindex = 100;
	static BasicNodeContainer currentlyOntop = null;
	
	public void setUpForClass(SSSNode node) {

		

		nodeLabel.setText(node.getPLabel());
		nodeValueURI.setText(node.getShortPURI());
		nodeValueURI.setTargetHistoryToken("<nodeUri:"+node.getPURI()+">");
		
		nodeValueURI.setTitle("parents:"+node.getDirectParentsAsString());

		dataHolder.setWidth("150px");

		dataHolder.add(nodeLabel);			
		dataHolder.add(nodeValueURI);
		dataHolder.add(new NodeResultLabel(node));
		//directParents.add(directParentsLabel);
		//directParentsLabel.setText(node.getDirectParentsAsString());
		nodeLabel.setTitle("labels:"+node.getAllPLabels());
		nodeLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		setupStyle();
	}

	private void setupStyle() {
		
		add(dataHolder);
		
		this.getElement().getStyle().setPadding(4, Unit.PX);
		this.getElement().getStyle().setBackgroundColor("#FEE");
		this.getElement().getStyle().setBorderColor("blue");
		this.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);

		
		//set up focus to top method
		this.addClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				
				if (currentlyOntop!=null){
					currentlyOntop.getElement().getParentElement().getStyle().setZIndex(100); 
				}
				
				if (thisContainer.isAttached()){
					thisContainer.getElement().getParentElement().getStyle().setZIndex(200);
					currentlyOntop=thisContainer;
				}
				
			}
		});
		
	}

	public BasicNodeContainer(SSSProperty prop) {

		if (prop.getPred()==SSSNode.SubClassOf){
			setUpForClass(prop.getValue());
			return;
		}

		
		//Label pURI =new Label(prop.getPred().getPURI());
		 
		nodePredURI.setText(prop.getPred().getShortPURI());
		nodePredURI.setTargetHistoryToken("<nodeUri:"+prop.getPred().getPURI()+">");		
		nodePredURI.setTitle("parents:"+prop.getPred().getDirectParentsAsString());
		
		//Label vURI =new Label(prop.getValue().getPURI());
		nodeValueURI.setText(prop.getValue().getShortPURI());
		nodeValueURI.setTargetHistoryToken("<nodeUri:"+prop.getValue().getPURI()+">");		
		nodeValueURI.setTitle("parents:"+prop.getValue().getDirectParentsAsString());
		
	//	pURI.setWordWrap(true);
		//nodeValueURI.setWordWrap(true);
		
		nodePredURI.getElement().getStyle().setWhiteSpace(WhiteSpace.PRE_WRAP);
		nodeValueURI.getElement().getStyle().setWhiteSpace(WhiteSpace.PRE_WRAP);
		
		Label comboLabel = new Label(prop.getPred().getPLabel()+"="+prop.getValue().getPLabel());
		comboLabel.getElement().getStyle().setFontWeight(FontWeight.BOLD);
		dataHolder.add(comboLabel);
		
		dataHolder.add(nodePredURI);
		dataHolder.add(nodeValueURI);

		setupStyle();
		
		
	}
	
	public void reAttach(){
		super.onAttach();
	}

	public String contentSummery(){
		
		return nodeValueURI.getText()+" "+nodeLabel.getText();
		
	}

	 
	/** Sets the background CSS
	 * If RESULT is specified it uses the result color, and doesnt allow it to change after this point **/
	public void setBackgroundColour(String cssOrRESULT){
		
		if (cssOrRESULT.equals("RESULT")){
			this.getElement().getStyle().setBackgroundColor("#00DD00");
			ColourFixed=true;
		} else if (ColourFixed==false) {
			this.getElement().getStyle().setBackgroundColor(cssOrRESULT);
		}
		
	}
}
package com.darkflame.client.gui;

import com.darkflame.client.semantic.SSSTriplet;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

class NeatTripletLabel extends HorizontalPanel  {
	
	Label toSubjectArrow = new Label("<---");
	Label propertySeperartion = new Label(" : ");
	
	Label subject = new Label("");
	Label predicate = new Label("");
	Label value = new Label("");
	
	public NeatTripletLabel(SSSTriplet displayThis) {		
		propertySeperartion.getElement().getStyle().setColor("grey");
		toSubjectArrow.getElement().getStyle().setColor("grey");
		predicate.getElement().getStyle().setColor("rgb(49, 139, 45)");
		
		
		add(subject);
		add(toSubjectArrow);		
		add(predicate);		
		add(propertySeperartion);
		add(value);
		
		
		if (displayThis.getSubject()!=null){
			subject.setText(displayThis.getSubject().getShortPURI());
			subject.setTitle(displayThis.getSubject().getAllPLabels());
		}
		if (displayThis.getPrecident()!=null){
			predicate.setText(displayThis.getPrecident().getShortPURI());
			predicate.setTitle(displayThis.getPrecident().getAllPLabels());
		}		
		if (displayThis.getValue()!=null){
			value.setText(displayThis.getValue().getShortPURI());
			value.setTitle(displayThis.getValue().getAllPLabels());
			
		}
	
	}
	
}
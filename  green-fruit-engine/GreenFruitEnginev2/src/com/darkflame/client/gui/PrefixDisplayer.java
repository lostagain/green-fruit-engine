package com.darkflame.client.gui;

import java.util.Iterator;

import com.darkflame.client.semantic.RawQueryUtilities;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PrefixDisplayer extends VerticalPanel
{
	final PrefixDisplayer thisDisplayer=this;
	
	public PrefixDisplayer() {
		super();
		Button refreshList = new Button("refresh list");
		this.add(refreshList);
		

		refreshList.addClickHandler( new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				PrefixDisplayer.refreshlist(thisDisplayer);
			}
		});
		
	}

	public static void refreshlist(PrefixDisplayer refreshthisOne){

		Iterator<String> Prefixs = RawQueryUtilities.getPrefixs().keySet().iterator();
		
		while (Prefixs.hasNext()) {
			
			String prefix = (String) Prefixs.next();
			String value = RawQueryUtilities.getPrefixs().get(prefix);

			refreshthisOne.add(new Label(value+" = "+prefix));
			
		}
		
		
		
	}
}

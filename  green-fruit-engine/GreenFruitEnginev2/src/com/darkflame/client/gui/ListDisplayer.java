package com.darkflame.client.gui;


import java.util.HashMap;
import java.util.logging.Logger;

import org.eclipse.jetty.util.log.Log;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/** used to display multiple lists side by side in a row **/
public class ListDisplayer extends FlowPanel {

	static Logger Log = Logger.getLogger("sss.ListDisplayer");
	HashMap<String,SingleList> allLists = new HashMap<String,SingleList>();
	
	HorizontalPanel contents = new HorizontalPanel();
	Widget headerWidget = null;
	
	public Widget getHeaderWidget() {
		return headerWidget;
	}

	public void setHeaderWidget(Widget headerWidget) {
		this.headerWidget = headerWidget;
		setup();
	}
	class SingleList extends VerticalPanel {

		String name="";
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.setTitle(name);
			this.name = name;
		}

		public SingleList(String title) {
			
			super();		
			name=title;
			Label titlelab = new Label(title);			
			titlelab.getElement().getStyle().setFontWeight(FontWeight.BOLD);
			
			createList(titlelab);
			
			
		}
		
		public SingleList(Widget firstWidgetInList) {
			
			super();			
			
			createList(firstWidgetInList);
			
			
		}

		private void createList(Widget firstWidgetInList) {
			

			this.add(firstWidgetInList);
			
			this.setWidth("400px");
			
			this.getElement().getStyle().setBackgroundColor("#CCFFCC");
			this.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
			
			//randomise colour slightly to help visual distinction
			int g=(int) (Math.random()*100)+150;
			int r=(int) (Math.random()*100)+150;
			this.getElement().getStyle().setBackgroundColor("rgb("+r+","+g+",255)");
			
		}
		
		
		
		
	}
	
	public ListDisplayer() {
		super();		
		//add a button for refresh
		this.setWidth("99%");
		this.setHeight("98%");
		this.getElement().getStyle().setOverflow(Overflow.SCROLL);
		
	//	ScrollPanel scroller = new ScrollPanel();
	//	scroller.setWidth("100%");
	//	scroller.setHeight("100%");
		
		//scroller.add(contents);
		setup();
		
		
	}

	public void setup()
	{
		clear();
		if (headerWidget!=null){
		add(headerWidget);
		}
		add(contents);
		
	}
	//we don't display straight away for dom performance reasons
	public void addList(SingleList newlist , String listName){
		
		if (allLists.containsKey(listName)){
			OverallInterface.debugDisplayer.info("ERROR: Already contained list of this type_two commonpropertysets exists for same property");
			String newlistName = listName;
			int i=2;
			while (allLists.containsKey(newlistName)){
			newlistName=listName+"("+i+")";
			i++;
			}
			
			listName = newlistName;
		}
		newlist.setName(listName);
		allLists.put(listName, newlist);		

		
	}
	
	//clear and then refill based on internal lists
	public void refreshListDisplay(){
		
		contents.clear();	
		
		OverallInterface.debugDisplayer.info("ListDisplayer refreshing");
		
		for (SingleList list : allLists.values()) {
			
			OverallInterface.debugDisplayer.info("Adding list:"+list.name);
			contents.add(list);
			
		} 
		
		
		
	}
	@Override
	public void clear(){
		contents.clear();
		//add to list sets
		allLists.clear();
		
		
	}
	
	
	
}

package com.darkflame.client.gui;

import com.darkflame.client.gui.ListDisplayer.SingleList;
import com.darkflame.client.interfaces.GenericIndexDisplayer;
import com.darkflame.client.semantic.SSSIndex;
import com.darkflame.client.semantic.SSSTriplet;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;

/** Displays all the known indexes **/
public class SSSIndexDisplayer extends ListDisplayer implements GenericIndexDisplayer {

	Button refreshButton = new Button("(refresh)");
	
	public SSSIndexDisplayer() {
				
		super();
		super.setHeaderWidget(refreshButton);
		refreshButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				refreshListDisplay();
			}
		});
		
	}

	
	public void addIndexToDisplayer(SSSIndex currentIndex) {
				
		SingleList newList = new SingleList("Name Space:"+currentIndex.getIndexsDefaultNameSpace()+"___"+
											"Url:"+currentIndex.IndexURL);
		
		newList.add(new Label(""));
		
		for (SSSTriplet line : currentIndex) {
		
			newList.add(new NeatTripletLabel(line));
			
		}
		
		this.addList(newList, currentIndex.getIndexsDefaultNameSpace());
		
		
	}

}

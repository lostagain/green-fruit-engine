package com.darkflame.client.gui;

import com.darkflame.client.interfaces.GenericProgressMonitor;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;

//The box the user types in to start their search
//Behind the box is a gradient bar
public class SearchBox extends AbsolutePanel implements GenericProgressMonitor  {

	TextBox userEnterBox = new TextBox();
	
	HorizontalPanel backgroundBar = new HorizontalPanel();
	SimplePanel colouredBit = new SimplePanel();
	
	double Total = 0;
	double CurrentStep =0;
	double CurrentPercent = 0;
	
	public SearchBox(){
		setup();
	}
	
	public void setup(){
		
		this.setStylePrimaryName("gwt-TextBox");
		this.getElement().getStyle().setPadding(0, Unit.PX);
		
		userEnterBox.removeStyleName("gwt-TextBox");
		
		add(backgroundBar,0,0);
		add(userEnterBox,0,0);
		
		userEnterBox.setWidth("100%");
		userEnterBox.setHeight("100%");
		userEnterBox.getElement().getStyle().setBackgroundColor("transparent");
		
		backgroundBar.add(colouredBit);
		
		backgroundBar.setWidth("100%");
		backgroundBar.setHeight("100%");
		
		colouredBit.setWidth("100%");
		colouredBit.setHeight("100%");
		
		colouredBit.getElement().getStyle().setBackgroundColor("rgb(186, 213, 255)");
		

		updateBar();
		
	}
	
	public void setText(String text){
		userEnterBox.setText(text);
	}
	public String getText(){
		return userEnterBox.getText();
	}
	
	public void setTotalSteps(double total){
		
		Total = total;
		
	}
	
	public void setCurrentStep(double Current){
		
		CurrentStep=Current;
		
		CurrentPercent = (CurrentStep/Total)*100.0;
			
		this.setTitle("CurrentPercent="+CurrentPercent+" ("+CurrentStep+" of "+Total+")");
		
		updateBar();
		
	}

	public void stepProgressForward(){
		
		CurrentStep++;
		
		CurrentPercent = (CurrentStep/Total)*100;
			
		updateBar();
		
	}
	public void updateBar(){
		
		//set percent
		colouredBit.setWidth(CurrentPercent+"%");
		
	}

	public void addKeyPressHandler(KeyPressHandler keyPressHandler) {
		
		userEnterBox.addKeyPressHandler(keyPressHandler);
		
	}

	public void addToTotalProgressUnits(int size) {

		CurrentStep=CurrentStep+size;
		
		CurrentPercent = (CurrentStep/Total)*100.0;
			
		this.setTitle("CurrentPercent="+CurrentPercent+" ("+CurrentStep+" of "+Total+")");
		
		updateBar();
		
	}
	
	
	public void setEnabled(boolean status){
		
		if (status==false){
			userEnterBox.setEnabled(false);
		} else {
			userEnterBox.setEnabled(true);
		}
		
		
	}

	@Override
	public void setCurrentProcess(String message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTotalProgressUnits(int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCurrentProgress(int i) {
		// TODO Auto-generated method stub
		
	}
}

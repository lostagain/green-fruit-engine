package com.darkflame.client.gui;

import com.darkflame.client.interfaces.GenericDebugDisplayer;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class DebugDisplayer extends FlowPanel implements GenericDebugDisplayer {
	

	VerticalPanel Overall = new VerticalPanel();
	VerticalPanel loglist = new VerticalPanel();
	ScrollPanel logScroller = new ScrollPanel();
	HorizontalPanel controls = new HorizontalPanel();
	
	Button clear = new Button("clear");
	Boolean timeStampOn = true;
	long startTime=0;
	long lastTime=0;
	
	public DebugDisplayer(){
		
		controls.add(clear);
		controls.setHeight("50px");
		Overall.setSize("100%", "100%");
		Overall.add(controls);
		Overall.setCellHeight(controls, "50px");
		
		Overall.add(logScroller);
		logScroller.setSize("100%", "100%");
		logScroller.add(loglist);
		
		startTime = System.currentTimeMillis();
		
		this.add(Overall);
		
		clear.addClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				loglist.clear();
			}
		});
		
	}
	
	public void addControl(Widget addThis){
		
		controls.add(addThis);
		
	}
	
	@Override
	public void setPixelSize(int w,int h){
				
		super.setPixelSize(w, h);
		logScroller.setPixelSize(w, h-controls.getOffsetHeight());
		
		
	}
	
	
	public void log(String logThis){
		
		logThis = addTimeStamp(logThis);
		
		logEntry newEntry = new logEntry(logThis);
		loglist.add(newEntry);
		logScroller.scrollToBottom();
	}
	
	private String addTimeStamp(String logThis) {

		
		if (timeStampOn){
			logThis=(System.currentTimeMillis()-startTime)+"  ("+(System.currentTimeMillis()-lastTime)+")    --"+logThis;
		}
		

		lastTime = System.currentTimeMillis();
		
		return logThis;
	}
	public void resetTimeStamp(String logThis) {
		
			startTime=System.currentTimeMillis();
			
		return;
		
	}
	
	public void info(String logThis){	

		logThis = addTimeStamp(logThis);		
		logEntry newEntry = new logEntry(logThis);
		
		loglist.add(newEntry);
		logScroller.scrollToBottom();
	}
	
	
	public void log(String logThis,String color){

		logThis = addTimeStamp(logThis);
		
		logEntry newEntry = new logEntry(logThis,color);
		loglist.add(newEntry);
		logScroller.scrollToBottom();
	}
	public void error(String logThis){

		logThis = addTimeStamp(logThis);
		
		logEntry newEntry = new logEntry(logThis,"#EE0000");
		
		loglist.add(newEntry);
		logScroller.scrollToBottom();
	}
	public void addWidgetToList(Widget addThis){
		
		loglist.add(addThis);
		
	}
	
	class logEntry extends HorizontalPanel {
		
		Label contentLabel = new Label("");
		
		public logEntry(String contents) {			
			contentLabel.setText(contents);		
			this.add(contentLabel);
		}
		public logEntry(String contents,String color) {			
			contentLabel.setText(contents);		
			contentLabel.getElement().getStyle().setColor(color);
			this.add(contentLabel);
		}
		
		public String getContents(){
			return contentLabel.getText();
		}
	}
	

}

package com.darkflame.client.gui;

import java.util.ArrayList;
import java.util.HashSet;

import com.darkflame.client.interfaces.GenericPropertyListDisplayer;
import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSNodesWithCommonProperty;
import com.google.common.collect.HashMultimap;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/** displays all loaded common property lists**/
public class CommonPropertyListDisplayer extends ListDisplayer implements GenericPropertyListDisplayer{

	private final HashSet<SSSNodesWithCommonProperty> ListsDisplayed = new HashSet<SSSNodesWithCommonProperty>();

	Button refreshButton = new Button("(refresh)");

	private HashMultimap<SSSNode, SSSNodesWithCommonProperty> database;

	public CommonPropertyListDisplayer() {
		super();
		
		super.setHeaderWidget(refreshButton);
		
		refreshButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				refreshLists();
			}
		});
		
		
		
	}
	
	public void removeCPLToDisplayer(SSSNodesWithCommonProperty cpl) {
		
		//OverallInterface.debugDisplayer.info("removing list:"+cpl.toString());
		
		//OverallInterface.debugDisplayer.info("-"+ListsDisplayed.toString());
		//
		//OverallInterface.debugDisplayer.info("-"+ListsDisplayed.remove(cpl));
		
		
	}
	public void addCPLToDisplayer(SSSNodesWithCommonProperty cpl) {
				
		//ListsDisplayed.add(cpl);
				
	//	Log.info("adding CPLToDisplayer:"+cpl.size());
		
		//create the list panel for the supplied SSSNodesWithCommonProperty object
		//SingleList newList = createSimpleListPanel(cpl);
		
	//	this.addList(newList, cpl.getCommonPrec().getShortPURI()+"_"+cpl.getCommonValue().getShortPURI());
		
		
		
	}
	public void refreshLists(){
		
		//clears existing panels
		clear();

		
		
		//recreate all panels we know of
		
		
		for (SSSNodesWithCommonProperty cpl_to_display : database.values()) {
			
			SingleList newList = createSimpleListPanel(cpl_to_display);			
			this.addList(newList, cpl_to_display.getCommonPrec().getShortPURI()+"_"+cpl_to_display.getCommonValue().getShortPURI());
			
		}
		
		//clears existing panels and redraws the list panels
		refreshListDisplay();

	}
	
	

	private SingleList createSimpleListPanel(SSSNodesWithCommonProperty cpl) {
		//make a neat first widget to sum up the information
		VerticalPanel CPLinfo = new VerticalPanel();
		
		Label titleExplanation = new Label("All nodes with:");
		titleExplanation.getElement().getStyle().setFontWeight(FontWeight.BOLD);		
		Label titleProperty = new Label(cpl.getCommonPrec().getShortPURI()+"<--"+cpl.getCommonValue().getShortPURI());
		
		Label titleExplanation2 = new Label("From uris:");
		titleExplanation2.getElement().getStyle().setFontWeight(FontWeight.BOLD);		
		Label titleUris = new Label(cpl.getAllDomainsSourced());
		
		Label titleExplanation3 = new Label("Source files:");
		titleExplanation3.getElement().getStyle().setFontWeight(FontWeight.BOLD);		
		Label titleUrls = new Label(cpl.getSourceFiles());
		
		CPLinfo.add(titleExplanation);
		CPLinfo.add(titleProperty);
		CPLinfo.add(titleExplanation2);
		CPLinfo.add(titleUris);
		CPLinfo.add(titleExplanation3);
		CPLinfo.add(titleUrls);
		
		//spacer
		CPLinfo.add(new Label("---"+cpl.size()));
		
		if (!cpl.isLoaded){
			CPLinfo.add(new Label("-- not loaded yet --"));
		}
		
		//make list
		SingleList newList = new SingleList(CPLinfo);
		
		newList.add(new Label(""));
		
		for (SSSNode line : cpl) {
			
			Hyperlink nodelink = new Hyperlink();
			nodelink.setText(line.getShortPURI());
			nodelink.setTargetHistoryToken("<nodeUri:"+line.getPURI()+">");
			
			//now we get all the uris that support that fact this node has this property (that is, it belongs in this predicate/value set)
			String supportinguris = cpl.getURIsThatSupportThisTriplet(line);
			nodelink.setTitle("labels:"+line.getAllPLabels()+" source uri:"+supportinguris);
			
			
			newList.add(nodelink);
			
		}
		return newList;
	}

	//Temp: The "remove" above isnt working, for now we can use a direct link to the internal SemanticDatabase
	//This is rather bad form though, as it allows this list to mess up the database.
	//GET should only be used on the "globalnodes..." variable, anything else is dangerious
	@Override
	public void setCPLDatabase(
			HashMultimap<SSSNode, SSSNodesWithCommonProperty> globalNodesWithPropertyListByPredicate) {
		
		database=globalNodesWithPropertyListByPredicate;
		
		
		
		
	}
}

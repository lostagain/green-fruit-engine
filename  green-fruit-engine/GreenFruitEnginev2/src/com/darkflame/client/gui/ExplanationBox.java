package com.darkflame.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SimplePanel;

public class ExplanationBox extends SimplePanel {

	AbsolutePanel contents = new AbsolutePanel();
	
	Frame explanationboxcontents = new Frame();
	
	Button exitButton = new Button("X");
	ExplanationBox thisbox=this;
	
	public ExplanationBox() {
		
		super();
		super.setSize("300px", "215px");
		super.getElement().getStyle().setBackgroundColor("white");
		this.add(contents);
		
		contents.setSize("100%", "100%");
		contents.add(explanationboxcontents,0,0);
		contents.add(exitButton,280,0);
		
		explanationboxcontents.setSize("100%", "100%");
		
		exitButton.addClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				thisbox.removeFromParent();
			}
		});
		
		//load the contents if theres any
		explanationboxcontents.setUrl("Docs/explanation_window.html");
		
		explanationboxcontents.addLoadHandler(new LoadHandler() {
			
			@Override
			public void onLoad(LoadEvent event) {
				
				//handle 404 here
				
			}
		});
		
		
		
		
	}

	
}

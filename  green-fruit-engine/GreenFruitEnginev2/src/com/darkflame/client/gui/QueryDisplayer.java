package com.darkflame.client.gui;

import com.darkflame.client.GreenFruitEnginev2;
import com.darkflame.client.SuperSimpleSemantics;
import com.darkflame.client.interfaces.GenericQueryDisplayer;
import com.darkflame.client.query.Query;
import com.darkflame.client.query.QueryElement;
import com.darkflame.client.query.Query.QueryMode;
import com.darkflame.client.semantic.SSSNode;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;




/** essentualy just a horizontal panel to display query data
 *  with a long thin empty panel under it to help debug, and highlight sub-query's **/

public class QueryDisplayer extends VerticalPanel implements GenericQueryDisplayer {
	
	
	HorizontalPanel queryVisuals = new HorizontalPanel();
	SimplePanel underline = new SimplePanel();
	
	class TypeSeperator extends Label{

		public TypeSeperator(String text) {
			super(text);
			//colour and spacing
			this.getElement().getStyle().setColor("grey");
			this.getElement().getStyle().setMargin(3, Unit.PX);
			
			
		}
		
		
		
	}
	
	
	
	public QueryDisplayer(Query queryToDisplay){

		super();

		setupWidget(queryToDisplay);
		
		populateWithQuery(queryToDisplay);
		

		
		
	}
	public void setupWidget(Query queryToDisplay) {
		this.add(queryVisuals);
		queryVisuals.clear();
		
		//associate it
		queryToDisplay.assoociatedDisplayer=this;
		
		//set up base bar
		underline.setWidget(new Label(" "));
		underline.setSize("100%", "10px");
		//underline.getElement().getStyle().setBackgroundColor("#444499");
	//	underline.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		underline.getElement().getStyle().setProperty("borderTop", "solid");
		
		this.add(underline);
	}
	public QueryDisplayer(){
		super();
	}
	
	@Override
	public void setResultData(String data){
		underline.setTitle(data);
	}
	
	@Override
	public void populateWithQuery(Query queryToDisplay) {
	
		//clear it
		this.clear();

		setupWidget(queryToDisplay);
		
		String typeSeperator = "";
		
		if (queryToDisplay.querymode==QueryMode.AND){
			 typeSeperator = " & ";
		}
		if (queryToDisplay.querymode==QueryMode.OR){
			 typeSeperator = " || ";
		}
		
		//if invert start with !
		if (queryToDisplay.invertResults){
			queryVisuals.add(new TypeSeperator("!"));
		}
		
		//we start with a open bracket
		queryVisuals.add(new TypeSeperator("("));
		
		
		for (QueryElement queryElement : queryToDisplay) {
			
			if (queryVisuals.getWidgetCount()>1){
				//we add a && or || inbetween each unless we are on the first queryElement
				queryVisuals.add(new TypeSeperator(typeSeperator));
			}
			
			if (queryElement.query!=null){
				
				QueryDisplayer newQuery = new QueryDisplayer(queryElement.query);
				//newQuery.setupWidget(queryElement.query);
				
				queryVisuals.add(newQuery);
				
			} else {
				
				SuperSimpleSemantics.info("------------(adding visuals for element)");							
				
				
				Widget predLab = new Widget();
				
				
				if (queryElement.prop.getPred()!=null){
					predLab = new Label(queryElement.prop.getPred().getPLabel());
					predLab.setTitle(queryElement.prop.getPred().PURI);
				}
				
				Widget valLab = new Widget();
				
				if (queryElement.prop.getValue()!=null){
					valLab = new Label(queryElement.prop.getValue().getPLabel());
					valLab.setTitle(queryElement.prop.getValue().PURI);
				}
				
				//if the predicate is a subquery, we override the label with a new subquery
				if (queryElement.prop.getQueryForPred()!=null){
					Query subqueryPredicate = queryElement.prop.getQueryForPred();
					predLab = new QueryDisplayer(subqueryPredicate);
				}	
				
				//if the predicate is a subquery, we override the label with a new subquery
				if (queryElement.prop.getQueryForValue()!=null){
					Query subqueryValue = queryElement.prop.getQueryForValue();
					valLab = new QueryDisplayer(subqueryValue);
				}
				
				//if its a error make it red
				if (queryElement.prop.getPred()==SSSNode.NOTFOUND){
					predLab.getElement().getStyle().setColor("red");
				}
				
				//if its a error make it red
				if (queryElement.prop.getValue()==SSSNode.NOTFOUND){
					valLab.getElement().getStyle().setColor("red");
				}
				
				
				queryVisuals.add(predLab);
				queryVisuals.add(new Label(":"));				
				queryVisuals.add(valLab);
			}
				
			
			
		}
		
		
		//we end with a close bracket
		queryVisuals.add(new TypeSeperator(")"));
		
	}
	@Override
	public void setQuery(Query queryToDisplay) {

		setupWidget(queryToDisplay);
		
		populateWithQuery(queryToDisplay);
		
	}

	
	

}

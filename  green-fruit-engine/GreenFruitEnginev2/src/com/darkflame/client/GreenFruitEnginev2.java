package com.darkflame.client;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.darkflame.client.gui.CommonPropertyListDisplayer;
import com.darkflame.client.gui.DebugDisplayer;
import com.darkflame.client.gui.OverallInterface;
import com.darkflame.client.gui.TrustedIndexList;
import com.darkflame.client.interfaces.GenericProgressMonitor;
import com.darkflame.client.interfaces.GenericPropertyListDisplayer;
import com.darkflame.client.interfaces.GenericWaitForRepeating;
import com.darkflame.client.semantic.LoadingCallback;
import com.darkflame.client.semantic.SSSIndex;
import com.darkflame.client.semantic.SSSNode;
import com.darkflame.client.semantic.SSSNodesWithCommonProperty;
import com.darkflame.client.semantic.demo.DemoKnowledgeBase;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 *  A visual online interface for querying SuperSimpleSemantic database's
 *  Lets you perform querys, see the breakdown of nodes from the various subquerys used to gather the results
 *  as well as displaying the final results in a dragable flowchart.
 *  Pretty much an essential tool for testing the SSS and debugging *.ntlist files.
 *  
 *  As this gets more developed and stable, hopefully it will also be a usefull tool for querying a vaste cross-sever database of knowledge too ;)
 *  
 *  Requires the "SuperSimpleSemantic" library.
 *  https://code.google.com/p/supersimplesemantics/
 *  
 *  Contributions, forks, and general poking around with this code, or the SSS very welcome
 *  https://code.google.com/p/green-fruit-engine/
 *  
 *  Also if you make your own ntlist files and want me to hock up the online demo to your databases, drop me an 
 *  message via the google code site
 *  
 *  Note; Code is a mess, sorry bout that
 *  
 *  @author Thomas Wrobel
 */
public class GreenFruitEnginev2 implements EntryPoint {

	static Logger Log = Logger.getLogger("gfe");

	//default ontology uri for when things are created within code.
	//Note Not used anymore, this is now dealt with in the semantic lib
	//final String baseURI = "examples/DefaultOntology.n3#";
	
	static OverallInterface overallInterface = new OverallInterface();

	
	@Override
	public void onModuleLoad() {
		Log.info("loading");
		
		//straight away we set up the super simple semantics
		SuperSimpleSemantics.setup();
		
		//assign the file manager SSS will use
		//As SSS is a Java lib, not GWT specific, this needs to be given with it
		//so it knows how to access files on severs
		SuperSimpleSemantics.setFileManager(new SpiffyFileManager());

		//set the default waitfor functions
		
		//These control user interface updates that happen during searchs
		//This allows the SSS lib to feel responsive, despite potentially doing
		//long complex, cpu intensive takes		
		SuperSimpleSemantics.setGenericWaitFor( new GWTWaitForBrowserToUpdate());		
		SuperSimpleSemantics.setGenericWaitForRepeating( new GWTWaitForRepeating());
		//-------------
		
		//Now we assign the code to trigger once the SSS's databases are loaded
		SuperSimpleSemantics.setLoadedRunnable(new Runnable() {			
			@Override
			public void run() {
				
				OverallInterface.debugDisplayer.log("Indexs Loaded", "Green");				
				postLoadActions();
				
			}
		});
					
		//finnaly add the main interface
		RootLayoutPanel.get().add(overallInterface); 
				
		
		//-------------------------------------------------------
		
		//set the correct debugger (now set in OverallInterface.java)
		//debugManager = new DebugDisplayer();
					
		//test some built in propertys, not needed unless things go wrong
		//DemoKnowledgeBase.test("oak");
		//DemoKnowledgeBase.test("plywood");

	
		
		
		
		//the below comments are outdated notes, sorry
		
		
		// get users preferences

		// preload all known index files (later can be a sub selection of them
		// if there's too many)
		// ready for query (activate box when complete)

		// Set up GUI to allow selection of specific indexs (preferences saved
		// to cookie)

		// on query:
		// Parse with hunchamatic to best-guess the request
		// statements are + by default - for things to exclude
		// Spaces are interpreted as "AND"

		// Figure out the URIs they most likely are referring too and list them
		// so the user can check
		//
		// get all propertys asked for (either to include or exclude) ie green

		// look over indexs for lists of things with this class (ie, all types
		// of green)

		// store in node file both the name and all known subtypes and
		// equivalents

		// iterate fill up the new node subtypes untill no new ones are found

		// now retrieve the triplets from the index file.
		// ie. all the stuff that is any of the types or subtypes of that above
		// (all green things, all fruit things etc)

		// once all triplets are retrieved and filled we process the query with
		// this new knowledge base

		// start with the inner most bracket, simply cross referencing nodes to
		// include/exclude
		// +isGreen +isFruit
		// add the results together from any ORs
		// iterate to next bracket set utill we reach the end

		// process results into node list
		// load equilivents as expansions in a expandable widget
		// as well as the domains each result was deduced from.

		// ===============

		// class's needed:
		// node file (stores label,uri,equilivents and subtypes)
		// triplet property list (stores all nodes with the same property)

		// result file (stores a node and the uri's the result was deduced from)

	}

	
	protected static void postLoadActions() {
		
		// after all the files are loaded we run this stuff
		overallInterface.postLoadInterfaceUpdates();
		
		
	}


	public static void gotoQueryPage() {
		overallInterface.selectTab(0);
		
	}

	public static void gotoLoadedPropertyListPage() {
		overallInterface.selectTab(2);
		
	}
	
	/** loads all the trusted indexs (*.ntlist) files which define the 
	 * databases. These lists can be hosted anywhere, and a sever side php script
	 * used to get around SOP issues if needed */
	public static void loadIndexsAt(ArrayList<String> trustedIndexs) {
							
		SuperSimpleSemantics.clearAllIndexsAndNodes();
		 		 
		//runnable needs to be re-added atm
		SuperSimpleSemantics.setLoadedRunnable(new Runnable() {			
			@Override
			public void run() {
				
				OverallInterface.debugDisplayer.log("Indexs Loaded", "Green");
				
				postLoadActions();
				
			}
		});
				
		SuperSimpleSemantics.loadIndexsAt(trustedIndexs);
		
		
	}

	
}

# The Green Fruit Engine #

This project specifically is a search engine for SuperSimpleSemantic data. It is complete client-side GWT, with a small bit of php that (optionally) can be used to deal with SOP issues. The GreenFruitEngine allows querys with subquerys AND ORS and negative operators, and outputs to a dragable flowchart.

demo
http://darkflame.co.uk/GreenFruitEngine2/GreenFruitEnginev3_new.html

Interested committers very welcome

# SuperSimpleSemantic #

(Please see the SuperSimpleSemantics own project for full details, a summery is below)

The SuperSimpleSemantic project aims to allow anyone to host data in the form of text-file lists, and then allow the semantic searching and deduction based on those lists.

For example, a list of fruit could be hosted on one server, and a list of green things on another. By looking at both lists the GFE can answer the query for "color=green fruit".

On a more technical level: Its a simple transitional n3-like system, only with the whole database folded so that endpoints arnt needed for cross referencing- static text files and indexs are enough for anyones data to be accessible by any client like this.

Updated: The core SuperSimpleSemantic engine is now pure generic java in a supplied .jar, code here; https://code.google.com/p/supersimplesemantics/. This means the core library be used for any java project, not just gwt webbased things.

My widgets to handle the dragpanel and loading icon now included.

You just need this and the SpiffyConnections (also on google code) Widget to compile.